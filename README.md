### Purpose of this repo

I just wanted to write some sample app to show my code quality. If you find mistakes in my code or something that can help me, feel free to send me a message at: marcin.skrzynski.7[at]gmail.com

### Starting

To install app run:
`npm install`
or
`yarn`

To start app run:
`$ yarn start`
or
`$ npm run start`

### Database

#### Connection with database

```javascript
import Database from "./FakeDatabase";
const database = new Database();
```

#### Creating table

```javascript
//create empty table
database.create("users");

//create table and add data
database.create("users", [{ id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }, { id: "c65c2e04-6965-40dd-93c9-d103e8407a8f", active: true }, { id: "54f41f85-d5df-42f9-b0ba-07de7314b2fb", active: false }]);
```

#### Querying data

```javascript
//getting all data from table
database.table("users").getAll();
// => [{ id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }, { id: "c65c2e04-6965-40dd-93c9-d103e8407a8f", active: true }, { id: "54f41f85-d5df-42f9-b0ba-07de7314b2fb", active: false }]

//or you can short that
database.all("users");
// => [{ id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }, { id: "c65c2e04-6965-40dd-93c9-d103e8407a8f", active: true }, { id: "54f41f85-d5df-42f9-b0ba-07de7314b2fb", active: false }]

//When you want to display all records matching query
database.table("users").get({ active: true});
// => [{ id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }, { id: "c65c2e04-6965-40dd-93c9-d103e8407a8f", active: true }]

//or using shorthand
database.table("users").get("d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3"); // Note that you need id column
// => [{ id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }]

//when you omit query you get all results
database.table("users").get(); // like database.table("users").getAll(); or database.all("users");
// => [{ id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }, { id: "c65c2e04-6965-40dd-93c9-d103e8407a8f", active: true }, { id: "54f41f85-d5df-42f9-b0ba-07de7314b2fb", active: false }]

//When you want to display first record matching query
database.table("users").getOne({ id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3" });
// => { id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }

//or using shorthand
database.table("users").getOne("d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3"); // Note that you need id column
// => { id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }
```

#### Adding data

```javascript
// adding new record to table
database.table("users").add({id: "9fb75b28-f378-4094-8fd7-005fd44cef6a", active: true}); // [{ id: "d6b4f324-f1fa-4f5d-afc0-49fc9e752bc3", active: true }, { id: "c65c2e04-6965-40dd-93c9-d103e8407a8f", active: true }, { id: "54f41f85-d5df-42f9-b0ba-07de7314b2fb", active: false }, { id: "9fb75b28-f378-4094-8fd7-005fd44cef6a", active: true }]
// => true
```

#### Removing data

```javascript
//removing record from table
database.table("users").remove({ active: true }); // [{ id: "54f41f85-d5df-42f9-b0ba-07de7314b2fb", active: false }]
// => true

//or using shorthand
database.table("users").remove("c65c2e04-6965-40dd-93c9-d103e8407a8f"); // Note that you need id column -> [{ id: "c65c2e04-6965-40dd-93c9-d103e8407a8f", active: true }, { id: "54f41f85-d5df-42f9-b0ba-07de7314b2fb", active: false }, { id: "9fb75b28-f378-4094-8fd7-005fd44cef6a", active: true }]
// => true

//when you omit query you remove all records
database.table("users").remove();  // []
// => true
// or
database.table("users").truncate();  // []
// => true
```

#### Removing table

```javascript
//removing table
database.table("users").delete(); // => true

//or using shorthand
database.delete("users"); // => true
```
