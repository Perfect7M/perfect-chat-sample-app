export default {
  socket: {
    port: 5001,
    host: "http://localhost"
  },
  snapshot: {
    watchTime: 10
  }
};
