import React, { Component } from "react";
import "./App.css";
import { Router } from "@reach/router";
import io from "socket.io-client";
import config from "./../config/client";
import { v4 as isUuid } from "is-uuid";

import Chat from "./../Chat/Chat";
import Snapshot from "./../Snapshot/Snapshot";
import Error from "./../Error/Error";

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      connected: false,
      joined: false,
      messages: [],
      error: {
        display: false,
        message: ""
      },
      userId: "",
      snapshots: []
    };

    //Object with lang texts
    this.lang = {
      cantJoinRoom: "You can't join this room",
      cantGetMediaStream: "We can't get access to your camera",
      connectionLost: "Connection to socket server is lost",
      snapshotSent: "Snapshot sent...",
      sendButton: "Send",
      takeSnapshotButton: "Take snapshot",
      checkNewSnapshtosText: "You have snapshots to watch",
      tryAgainButton: "Try again",
      quitButton: "Quit",
      nextButton: "Next"
    };

    //redux like actions creators
    this.actions = {
      sendMessage: this.sendMessage.bind(this),
      cantGetMediaStream: this.cantGetMediaStream.bind(this),
      sendSnapshot: this.sendSnapshot.bind(this)
    };

    this.socket = io(`${config.socket.host}:${config.socket.port}`);
    this.socket.on("connect", this.onConnect.bind(this));
    this.socket.on("disconnect", this.onDisconnect.bind(this));
  }

  /**
   * When connection with web socket server is established listen to events
   * and save userId for later use
   */
  onConnect() {
    this.setState({ connected: true, userId: this.socket.id });

    this.socket.on(this.socket.id, data => this.setState({ ...data }));

    this.socket.on("message", message => {
      this.setState({ messages: [...this.state.messages, message] });
    });

    this.socket.on("notification", notification => {
      this.setState({ messages: [...this.state.messages, notification] });
    });

    this.socket.on("snap", snapshot => {
      this.setState({ snapshots: [...this.state.snapshots, snapshot] });
    });
  }

  /**
   * When user disconnect make sure display this to user
   */
  onDisconnect() {
    this.setState({ connected: false });
  }

  /**
   * When App is mounted join room
   * This cause re-rendering but we must set state so we need to wait for
   * component to be mounted
   */
  componentDidMount() {
    this.joinRoom();
  }

  /**
   * Send message across web socket to evry user join room
   */
  sendMessage(message) {
    this.socket.emit("send", {
      room: this.state.room,
      userId: this.state.userId,
      message: message
    });
  }

  /**
   * Send message across web socket to users who subscribed to room
   */
  sendSnapshot(snapshot) {
    this.socket.emit("snapshot", {
      room: this.state.room,
      userId: this.state.userId,
      snapshot: snapshot
    });
  }

  /**
   * Display error when access to camera is not granted
   */
  cantGetMediaStream() {
    this.displayError(this.lang["cantGetMediaStream"]);
  }

  /**
   * Display error to user
   */
  displayError(message) {
    this.setState({ error: { display: true, message: message } });
    return false;
  }

  /**
   * Joining room
   */
  joinRoom() {
    //We are in top level and don't know room uuid
    //We must get it from pathname and then join room
    const regexp = /\/chat\/([a-z0-9\-]+)/;
    const match = location.toString().match(regexp);
    const roomId = match[1];

    if (!isUuid(roomId)) {
      return this.displayError(this.lang["cantJoinRoom"]);
    }

    this.setState({ room: roomId });
    this.socket.emit("join", roomId);
  }

  render() {
    return (
      <div className="App">
        <div className="App-container">
          {!this.state.error.display ? (
            <Router>
              <Chat
                path="/:uuid"
                actions={this.actions}
                socket={this.socket}
                userId={this.state.userId}
                messages={this.state.messages}
                snapshots={this.state.snapshots}
                lang={this.lang}
              />
              <Snapshot
                path="/:uuid/snapshot"
                actions={this.actions}
                socket={this.socket}
                lang={this.lang}
              />
            </Router>
          ) : (
            <Error message={this.state.error.message} />
          )}
        </div>
      </div>
    );
  }
}

export default App;
