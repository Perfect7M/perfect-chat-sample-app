import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Error.css";

class Error extends Component {
  render() {
    const { message } = this.props;
    return <div className="Error">{message}</div>;
  }
}

Error.propTypes = {
  message: PropTypes.string.isRequired
};

export default Error;
