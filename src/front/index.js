import React from "react";
import ReactDOM from "react-dom";
import { Router } from "@reach/router";
import "./index.css";
import Home from "./Home/Home";
import App from "./App/App";
import registerServiceWorker from "./registerServiceWorker";

ReactDOM.render(
  <Router>
    <Home path="/" />
    <App path="/chat/*" />
  </Router>,
  document.getElementById("app")
);

registerServiceWorker();
