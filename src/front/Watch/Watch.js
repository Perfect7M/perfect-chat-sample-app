import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Watch.css";

import Button from "./../Button/Button";

class Watch extends Component {
  constructor(props) {
    super(props);

    const { startTimer } = props;

    startTimer();
  }

  render() {
    const { snapshot, onClose, timer, nextSnapshot, nextText } = this.props;
    return (
      <div className="Watch">
        <div className="Watch-close" onClick={onClose}>
          &times;
        </div>
        <div className="Watch-snapshot">
          <img src={snapshot.content} className="Watch-image" />
          <div className="Watch-toolbox">
            <div className="Watch-toolbox-left">{timer} s</div>
            <div className="Watch-toolbox-right">
              <Button onClick={nextSnapshot}>{nextText}</Button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

Watch.propTypes = {
  startTimer: PropTypes.func.isRequired,
  snapshot: PropTypes.object.isRequired,
  onClose: PropTypes.func.isRequired,
  timer: PropTypes.number.isRequired,
  nextSnapshot: PropTypes.func.isRequired,
  nextText: PropTypes.string.isRequired
};

export default Watch;
