import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Snapshot.css";
import io from "socket.io-client";
import config from "./../config/client";

import Webcam from "react-webcam";
import Button from "./../Button/Button";

class Snapshot extends Component {
  constructor(props) {
    super(props);

    this.state = {
      image: ""
    };

    this.capture = this.capture.bind(this);
    this.setRef = this.setRef.bind(this);
    this.tryAgain = this.tryAgain.bind(this);
    this.onError = this.onError.bind(this);
    this.send = this.send.bind(this);
  }

  /**
   * Setting reference
   */
  setRef(webcam) {
    this.webcamRef = webcam;
  }

  /**
   * Capture picture from user's camera
   */
  capture() {
    const imageSrc = this.webcamRef.getScreenshot();

    this.setState({ image: imageSrc });
  }

  /**
   * Clear saved image and try again caputre another
   */
  tryAgain() {
    this.setState({ image: "" });
  }

  /**
   * Fire this event when user not give access to camera
   */
  onError() {
    const { cantGetMediaStream } = this.props.actions;

    cantGetMediaStream();
  }

  /**
   * Save picture (snapshot) in state and send message that
   * user take picture
   */
  send() {
    const { sendSnapshot, sendMessage } = this.props.actions;
    const { navigate, lang } = this.props;

    sendSnapshot(this.state.image);
    sendMessage(lang.snapshotSent);

    navigate("../");
  }

  renderWebcam() {
    return (
      <Webcam
        audio={false}
        height={"100%"}
        ref={this.setRef}
        screenshotFormat="image/jpeg"
        width={"100%"}
        onUserMediaError={this.onError}
      />
    );
  }

  renderSnapshotToolButtons() {
    const { lang } = this.props;

    return (
      <React.Fragment>
        <Button className="Snapshot-send-button" onClick={this.send}>
          {lang.sendButton}
        </Button>
        <Button className="Snapshot-tryAgain-button" onClick={this.tryAgain}>
          {lang.tryAgainButton}
        </Button>
      </React.Fragment>
    );
  }

  renderSnapshotCaptureButton() {
    const { lang } = this.props;
    
    return (
      <Button className="block Snapshot-take-button" onClick={this.capture}>
        {lang.takeSnapshotButton}
      </Button>
    );
  }

  render() {
    const { lang } = this.props;

    return (
      <div className="Snapshot">
        <div className="Snapshot-container">
          {this.renderWebcam()}

          {this.state.image !== "" && (
            <div className="Snapshot-captured-image">
              <img src={this.state.image} className="Snapshot-image" />
            </div>
          )}
        </div>
        <div className="Snapshot-toolbox">
          <div className="Snapshot-toolbox-left">
            {this.state.image === ""
              ? this.renderSnapshotCaptureButton()
              : this.renderSnapshotToolButtons()}
          </div>
          <div className="Snapshot-toolbox-right">
            <Button to={`../`} className="block Snapshot-back-button">
              {lang.quitButton}
            </Button>
          </div>
        </div>
      </div>
    );
  }
}

Snapshot.propTypes = {
  socket: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  lang: PropTypes.object.isRequired
};

export default Snapshot;
