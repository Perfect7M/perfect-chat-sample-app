import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Button.css";
import { Link } from "@reach/router";

class Button extends Component {
  render() {
    const { children, onClick, className, to, type } = this.props;

    let universalProps = {
      className: `Button ${className ? className : ""}`
    };

    if (onClick) {
      universalProps.onClick = onClick;
    }

    if (type) {
      universalProps.type = type;
    }

    if (to) {
      universalProps.to = to;
    }

    return to ? (
      <Link {...universalProps}>{children}</Link>
    ) : (
      <React.Fragment>
        {type ? (
          <button {...universalProps}>{children}</button>
        ) : (
          <span {...universalProps}>{children}</span>
        )}
      </React.Fragment>
    );
  }
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func,
  className: PropTypes.string,
  to: PropTypes.string,
  type: PropTypes.string
};

export default Button;
