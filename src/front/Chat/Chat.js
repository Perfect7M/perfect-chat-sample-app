import React, { Component } from "react";
import "./Chat.css";
import PropTypes from "prop-types";
import config from "./../config/client";

import Message from "./../Message/Message";
import Textarea from "./../Textarea/Textarea";
import Button from "./../Button/Button";
import Watch from "./../Watch/Watch";

class Chat extends Component {
  constructor(props) {
    super(props);

    this.state = {
      snapshots: [],
      watchSnapshots: false,
      watchTimer: config.snapshot.watchTime
    };

    this.onSubmit = this.onSubmit.bind(this);
    this.setRef = this.setRef.bind(this);
    this.closeWatchOverlay = this.closeWatchOverlay.bind(this);
    this.showWatchOverlay = this.showWatchOverlay.bind(this);
    this.nextSnapshot = this.nextSnapshot.bind(this);
    this.startTimer = this.startTimer.bind(this);
    this.stopTimer = this.stopTimer.bind(this);
    this.nextSnapshot = this.nextSnapshot.bind(this);

    this.watchTimer = null;
  }

  /**
   * Handle form submision
   */
  onSubmit(e) {
    e.preventDefault();

    const $textarea = e.target.querySelector("textarea");
    let value = $textarea.value;
    value = value.trim();

    if (value) {
      const { sendMessage } = this.props.actions;

      sendMessage(value);

      $textarea.value = "";
    }
  }

  getSnapshotBeforeUpdate(prevProps, prevState) {
    if (prevProps.messages.length < this.props.messages.length) {
      const list = this.messagesListRef;
      return list.scrollHeight - list.scrollTop;
    }

    return null;
  }

  componentDidUpdate(prevProps, prevState, snapshot) {
    if (snapshot !== null) {
      const list = this.messagesListRef;
      list.scrollTop = list.scrollHeight - snapshot;
    }

    if (prevProps.snapshots.length < this.props.snapshots.length) {
      //new snapshots is comming
      const newSnapshots = [];

      this.props.snapshots.forEach(snapshot => {
        if (snapshot.userId !== this.props.userId) {
          return newSnapshots.push(snapshot);
        }
      });

      this.setState({ snapshots: [...this.state.snapshots, ...newSnapshots] });
    }
  }

  /**
   * Setting message list reference
   */
  setRef(messagesList) {
    this.messagesListRef = messagesList;
  }

  /**
   * Close watching snapshots overlay on user demand
   */
  closeWatchOverlay() {
    this.stopTimer(); //we must stop timer when closing overlay

    this.setState({ watchSnapshots: false });
  }

  /**
   * Show overlay where user can watch snapshots
   */
  showWatchOverlay() {
    this.setState({ watchSnapshots: true });
  }

  /**
   * Stoping timer
   */
  stopTimer() {
    window.clearTimeout(this.watchTimer);
  }

  /**
   * Starting snapchat-like watching countdown
   */
  startTimer() {
    if (this.state.watchTimer === 0) {
      this.nextSnapshot();
    } else {
      this.watchTimer = window.setTimeout(() => {
        let seconds = this.state.watchTimer;
        this.setState({ watchTimer: --seconds });

        this.startTimer();
      }, 1000);
    }
  }

  /**
   * Skipping snapshot
   */
  nextSnapshot() {
    this.stopTimer();

    const snapshots = this.state.snapshots;
    snapshots.shift();

    if (snapshots.length === 0) {
      this.closeWatchOverlay();
      this.setState({ snapshots: [] });
    } else {
      this.setState({
        snapshots: snapshots,
        watchTimer: config.snapshot.watchTime
      });

      this.startTimer();
    }
  }

  renderMessages(message) {
    const { userId } = this.props;
    
    return (
      <Message
        key={message.id}
        content={message.content}
        userId={userId}
        isOwn={message.userId === userId}
      />
    );
  }

  renderWatch() {
    const { lang } = this.props;
    
    return (
      <Watch
        onClose={this.closeWatchOverlay}
        onNext={this.nextSnapshot}
        timer={this.state.watchTimer}
        startTimer={this.startTimer}
        nextSnapshot={this.nextSnapshot}
        snapshot={this.state.snapshots[0]}
        nextText={lang.nextButton}
      />
    );
  }

  render() {
    const { messages, lang } = this.props;

    return (
      <div className="Chat">
        {this.state.snapshots.length > 0 && (
          <div className="Chat-new-snapshot" onClick={this.showWatchOverlay}>
            {lang.checkNewSnapshtosText} [{this.state.snapshots.length}]
          </div>
        )}
        {this.state.watchSnapshots && this.renderWatch()}
        <div ref={this.setRef} className="Chat-messages">
          {messages.map(message => this.renderMessages(message))}
        </div>

        <div className="Chat-toolbox">
          <form onSubmit={this.onSubmit}>
            <div className="Chat-toolbox-left">
              <Textarea />
            </div>
            <div className="Chat-toolbox-right">
              <div className="Chat-send">
                <Button className="block Chat-send-button" type="submit">
                  {lang.sendButton}
                </Button>
              </div>
              <div className="Chat-icons">
                <Button className="block Chat-snapshot-button" to="snapshot">
                  {lang.takeSnapshotButton}
                </Button>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }
}

Chat.propTypes = {
  messages: PropTypes.array.isRequired,
  snapshots: PropTypes.array.isRequired,
  lang: PropTypes.object.isRequired,
  userId: PropTypes.string.isRequired,
  socket: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
};

export default Chat;
