import React, { Component } from "react";
import Button from "../Button/Button"
import "./Home.css";

class Home extends Component {
  constructor() {
    super();

    this.lang = {
      heading: "Perfect Chat App",
      subheading: "lorem ipsum dolor sit amet lorem ipsum dolor sit amet",
      startButton: "Start chatting"
    };
  }
  render() {
    const { lang } = this;
    
    return (
      <div className="Home">
        <div className="Home-icon">
          <div className="Home-app-icon" />
        </div>
        <div className="Home-intro">
          <div className="Home-heading">{lang.heading}</div>
          <div className="Home-subheading">{lang.subheading}</div>
        </div>
        <div className="Home-button-container">
          <a href="/chat" className="Home-button">
            {lang.startButton}
          </a>
        </div>
      </div>
    );
  }
}

export default Home;
