import React, { Component } from "react";
import PropTypes from "prop-types";
import "./Message.css";
import { v4 as isUUID } from "is-uuid";

class Message extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { userId, content, isOwn } = this.props;

    return (
      <div className="Message">
        <div className={`Message-content ${isOwn ? "is-you" : "is-stranger"}`}>
          {content}
        </div>
      </div>
    );
  }
}

Message.propTypes = {
  userId: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  isOwn: PropTypes.bool.isRequired
};

export default Message;
