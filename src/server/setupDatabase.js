const setupDatabase = database => {
  database.create("rooms", [
    { id: "8281e10e-5b76-44ff-82ac-cfe76fbaa6a4", users: [], userLimit: 2 }
  ]);
  database.create("messages", [
    {
      id: 1,
      roomId: "8281e10e-5b76-44ff-82ac-cfe76fbaa6a4",
      message: "Hello world",
      userId: ""
    }
  ]);
  database.create("snapshots", [
    {
      id: 1,
      roomId: "8281e10e-5b76-44ff-82ac-cfe76fbaa6a4",
      snapshot: "Hello world",
      userId: ""
    }
  ]);

  return database;
};

export default setupDatabase;
