import { isEmpty, find, findIndex, filter, remove, isMatch } from "lodash";
import { v4 as isUuid } from "is-uuid";
/**
 * This is just fake database wrapper.
 * In this example we do not need connection to database.
 * This is only very primitive sample. If you want real database managment
 * take a look at ODM http://mongoosejs.com/ or ORM http://docs.sequelizejs.com/
 *
 */
export default class FakeDatabase {
  constructor() {
    this.database = {
      //this is our database
    };
  }

  /**
   * Check if table exist then set cursor on it
   */
  table(table) {
    if (this.database.hasOwnProperty(table)) {
      this.cursor = table;
      return this;
    }
  }

  /**
   * Get record with matching query
   */
  getOne(query = null) {
    //If we use shrothand Database.table(table).getOne(id) where uuid is id
    if (isUuid(query)) {
      query = { id: query };
    }

    return this.findOne(query);
  }

  /**
   * Check if record with matching query exist
   */
  exist(query = null) {
    //If we use shrothand Database.table(table).has(id) where uuid is id
    if (isUuid(query)) {
      query = { id: query };
    }

    return this.findOne(query) === undefined ? false : true;
  }

  /**
   * Get all records matching query
   * When there is not any query return all records
   */
  get(query = null) {
    if (!query) {
      return this.getAll();
    }

    //If we use shrothand Database.table(table).get(id) where uuid is id
    if (isUuid(query)) {
      query = { id: query };
    }

    return this.findAll(query);
  }

  /**
   * Find record with specified query
   */
  findOne(query) {
    return find(this.database[this.cursor], query);
  }

  /**
   * Find all records matching query
   */
  findAll(query) {
    return filter(this.database[this.cursor], query);
  }

  /**
   * For consitence of input we need wrap findIndex function to support
   * for number as query
   */
  getIndex(query) {
    //If we use shrothand Database.table(table).getIndex(1) where uuid is id
    if (isUuid(query)) {
      query = { id: query };
    }

    return findIndex(this.database[this.cursor], query);
  }

  /**
   * Get all records for table
   */
  getAll() {
    return this.database[this.cursor];
  }

  /**
   * Alias for Database.table(table).get();
   */
  all(table) {
    return this.table(table).getAll();
  }

  /**
   * Updating existing record
   */
  update(query, values) {
    const record = this.getOne(query);
    const recordIndex = this.getIndex(query);

    //adding record's proporties and overwrite it with changed
    this.database[this.cursor][recordIndex] = { ...record, ...values };

    return this.database[this.cursor][recordIndex];
  }

  /**
   * Adding new value to table
   */
  add(values) {
    this.database[this.cursor].push(values);
  }

  /**
   * Creating new table
   */
  create(table, initialValues = []) {
    this.database[table] = initialValues;
    return true;
  }

  /**
   * Removing all records matching query
   */
  remove(query) {
    if (isUuid(query)) {
      query = { id: query };
    }

    remove(this.database[this.cursor], record => {
      return isMatch(record, query);
    });

    return true;
  }

  /**
   * Alias to truncate all values from table
   */
  truncate() {
    this.remove();
  }

  /**
   * Delete table from database
   */
  delete(table = null) {
    this.cursor = undefined;
    return true;
  }
}
