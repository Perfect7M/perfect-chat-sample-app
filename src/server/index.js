import express from "express";
import bodyParser from "body-parser";
import cors from "cors";
import webpack from "webpack";
import webpackDevMiddleware from "webpack-dev-middleware";
import webpackConfig from "../../config/dev.webpack.js";
import serverConfig from "./config/server";
import clientConfig from "../front/config/client";
import http from "http";
import SocketIO from "socket.io";
import uuid from "uuid/v4";
import Database from "./FakeDatabase";
import setupDatabase from "./setupDatabase";
import path from "path";
import Chat from "./Chat";

//Setting up express server
const app = express();
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(express.static("/"));

//Setting up webpack dev server
const compiler = webpack(webpackConfig);
app.use(
  webpackDevMiddleware(compiler, {
    publicPath: webpackConfig.output.publicPath
  })
);

//Setting up database
const database = new Database();
const db = setupDatabase(database);
console.log("Database setup sucessfully");

const chat = new Chat(db);

//Setting up socket server
const server = http.createServer();
const io = SocketIO(server);

io.on("connection", function(client) {
  client.on("join", data => chat.join(data, client.id, client));
  client.on("send", data => chat.send(data, client.id, this));
  client.on("snapshot", data => chat.snapshot(data, client.id, this));
  client.on("disconnecting", () => chat.leave(client.id, client));
});

app.get("/chat", (req, res) => {
  const id = uuid();
  res.redirect(`/chat/${id}`);
});

app.use("*", function(req, res, next) {
  var filename = path.join(compiler.outputPath, "index.html");
  compiler.outputFileSystem.readFile(filename, function(err, result) {
    if (err) {
      return next(err);
    }

    res.set("content-type", "text/html");
    res.send(result);
    res.end();
  });
});

server.listen(clientConfig.socket.port, () => {
  console.log(
    `Socket server is up. Listening on port: ${clientConfig.socket.port}`
  );
});

const appServerPort = process.env.PORT || serverConfig.port;
app.listen(appServerPort, () => {
  console.log(`App server is up. Listening on port: ${appServerPort}`);
});
