import { v4 as isUuid } from "is-uuid";
import generateUUID from "uuid/v4";

export default class Chat {
  constructor(database) {
    this.db = database;

    //langugage lines
    this.lang = {
      maxUserExceed: "Max user exceed in this room.",
      roomJoinedSuccessfully: "Room joined successfully.",
      noPermission: "You can't send messages in this room",
      internalError: "Something went wrong :("
    };
  }

  /**
   * Sending message
   */
  send(data, userId, socket) {
    if (!this.isUserJoinedRoom(userId, data.room)) {
      return this.notifyUser(socket, userId, {
        message: this.lang.noPermission,
        error: true
      });
    }

    const messageId = generateUUID();

    this.db.table("messages").add({
      id: messageId,
      roomId: data.room,
      message: data.message,
      userId: data.userId
    });

    socket.to(data.room).emit("message", {
      id: messageId,
      content: data.message,
      userId: userId
    });
  }

  /**
   * Sending snapshot (picture)
   */
  snapshot(data, userId, socket) {
    if (!this.isUserJoinedRoom(userId, data.room)) {
      return this.notifyUser(socket, userId, {
        message: this.lang.noPermission,
        error: true
      });
    }

    const messageId = generateUUID();

    this.db.table("snapshots").add({
      id: messageId,
      roomId: data.room,
      snapshot: data.snapshot,
      userId: data.userId
    });

    socket.to(data.room).emit("snap", {
      id: messageId,
      content: data.snapshot,
      userId: userId
    });
  }

  /**
   * User join chat room. If room exist then we need add him to room users
   * otherwise new room is created
   */
  join(roomId, userId, client) {
    if (!isUuid(roomId)) {
      return false;
    }

    const roomExist = this.db.table("rooms").exist(roomId);

    if (!roomExist) {
      return this.createRoom(roomId, userId, client);
    }

    const room = this.db.table("rooms").getOne(roomId);

    if (room.users.length >= room.userLimit) {
      return this.notifyUser(client, userId, {
        message: this.lang.maxUserExceed,
        joined: false,
        error: true
      });
    }

    client.join(roomId, err => {
      if (err) {
        console.error(err);
      }

      room.users.push(userId);

      this.db.table("rooms").update({ id: roomId }, room);

      this.notifyUser(client, userId, {
        message: this.lang.roomJoinedSuccessfully,
        joined: true
      });

      this.sendRoomNotification(client, roomId, `${userId}  join room...`);
    });

    return true;
  }

  /**
   * When user disconnect from server we need kick him out from room users
   */
  leave(userId, client) {
    const connectedRooms = Object.keys(client.rooms);
    //Get all rooms where user is connected
    const rooms = [];
    connectedRooms.forEach(
      value => (isUuid(value) ? rooms.push(value) : false)
    );

    if (rooms.length === 0) {
      return false;
    }

    //We need update every room user joined and kick him out
    rooms.forEach(room => {
      const roomRecord = this.db.table("rooms").getOne({ id: room });
      const userIndex = roomRecord.users.indexOf(userId);

      if (userIndex === -1) {
        return false;
      }

      roomRecord.users.splice(userIndex, 1);
      this.db.table("rooms").update({ id: room }, roomRecord);

      this.sendRoomNotification(client, room, `${userId}  left room...`);
    });

    return true;
  }

  createRoom(roomId, userId, socket, roomLimit = 2) {
    this.db.table("rooms").add({ id: roomId, users: [], userLimit: roomLimit });

    return this.join(roomId, userId, socket);
  }

  /**
   * Check if user has joined room and is able to send messages or perform
   * other actions
   */
  isUserJoinedRoom(userId, roomId) {
    const room = this.db.table("rooms").getOne({ id: roomId });

    if (!room) {
      return false;
    }

    return room.users.includes(userId);
  }

  /**
   * Send message on user channel
   */
  notifyUser(socket, userId, message) {
    socket.emit(userId, message);
    return false;
  }

  /**
   * Send notification to room users
   */
  sendRoomNotification(socket, roomId, message) {
    const notificationId = generateUUID();

    socket.to(roomId).emit("notification", {
      id: notificationId,
      content: message,
      userId: "System"
    });

    return false;
  }
}
